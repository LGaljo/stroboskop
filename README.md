# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:
git clone https://LGaljo@bitbucket.org/LGaljo/stroboskop.git

Naloga 6.2.3:
https://bitbucket.org/LGaljo/stroboskop/commits/558da97b1e6f1cbc54ce6c9e9d42549f252a2049?at=master

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/LGaljo/stroboskop/commits/b3e3c70479e6c40a4d72da413be26cf3d6f0ff5e?at=izgled

Naloga 6.3.2:
https://bitbucket.org/LGaljo/stroboskop/commits/65c75b7781a12e424651f202ed4453c8c22c103c?at=izgled

Naloga 6.3.3:
https://bitbucket.org/LGaljo/stroboskop/commits/13c2a595ad24fe5b252d7cc48b48da1bb9ade36d?at=izgled

Naloga 6.3.4:
https://bitbucket.org/LGaljo/stroboskop/commits/77121a82cc3e3a5bf3cb6a1312954f5a77cfd722?at=izgled

Naloga 6.3.5:
git checkout master
git merge izgled
git push origin master

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/LGaljo/stroboskop/commits/54bbecfb88eaebc73156bcfd3556c4e228037c3a?at=master

Naloga 6.4.2:
https://bitbucket.org/LGaljo/stroboskop/commits/219f5c82209c32a779311c009231715d5c88f61d?at=master

Naloga 6.4.3:
https://bitbucket.org/LGaljo/stroboskop/commits/dcf2b98a1357dc93b70441345bc7456acc5a6a02?at=master

Naloga 6.4.4:
https://bitbucket.org/LGaljo/stroboskop/commits/74f4c291442556df62300ee33e82398a6779b0c5?at=master